package uk.co.melia;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import net.jawr.web.servlet.JawrServlet;

@SpringBootApplication
public class JawrBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JawrBootApplication.class, args);
	}
	
	@Bean(name = "JavascriptServlet")
	public ServletRegistrationBean createJsJawrServlet() {
		ServletRegistrationBean bean = createCommonJawrServlet();
		bean.setName("JavascriptServlet");
		bean.getInitParameters().put("type", "js");
		bean.setUrlMappings(Arrays.asList("*.js"));
		return bean;
	}

	private ServletRegistrationBean createCommonJawrServlet() {
		ServletRegistrationBean bean = new ServletRegistrationBean();
		bean.setLoadOnStartup(1);
		bean.getInitParameters().put("configLocation", "/jawr.properties");
		// bean.getInitParameters().put("configPropertiesSourceClass",
		// "uk.co.td.core.util.TDJawrMultiFileConfigSource");
		bean.setServlet(new JawrServlet());
		return bean;
	}
}
